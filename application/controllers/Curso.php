<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends CI_Controller {

   function __construct()
	{
		parent::__construct();
	}
  
	public function index()
	{
    $data["view"] = "dashboard";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
  
  public function formulario()
	{
    $data["view"] = "formulario";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
  
  public function control()
	{
    $data["view"] = "control";
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('layout', $data);
	}
	
	public function basicos($params = 'index')
	{
    $data["view"] = $params;
    // $data["iduser"] = $this->session->id_user;
		$this->load->view('/basicos/' . $params, $data);
	}
	
	
}
